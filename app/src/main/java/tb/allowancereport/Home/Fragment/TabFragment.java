package tb.allowancereport.Home.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.tabs.TabLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import tb.allowancereport.Home.HomeActivity;
import tb.allowancereport.Home.Model.ModelAllowance;
import tb.allowancereport.Home.Model.ModelExpenses;
import tb.allowancereport.R;

import static tb.allowancereport.Home.Fragment.ViewAllowanceFragment.allowanceListFilter;
import static tb.allowancereport.Home.Fragment.ViewFragment.expensesListFilter;

public class TabFragment extends Fragment implements View.OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private ImageView btnBack;
    private TextView mTitle;
    private DatePickerBuilder builder;
    private Context context;
    private DatePicker datePicker;
    private LinearLayout line1;
    private SearchView searchView;
    private String text;
    public static TextView tvStartDate, tvEndDate, tvbtnClose;


    public TabFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        View view = inflater.inflate(R.layout.fragment_tab, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    private void initView(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        btnBack = view.findViewById(R.id.btnBack);
        mTitle = view.findViewById(R.id.mTitle);
        line1 = view.findViewById(R.id.line1);
        tvStartDate = view.findViewById(R.id.tvStartDate);
        tvEndDate = view.findViewById(R.id.tvEndDate);
        tvEndDate = view.findViewById(R.id.tvEndDate);
        tvbtnClose = view.findViewById(R.id.tvbtnClose);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar();
        mTitle.setText(R.string.view_expenses);
        viewPager = view.findViewById(R.id.viewPager);
        setUpWithViewPager(viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(1).select();
        initListener();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                mTitle.setText(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initListener() {
        btnBack.setOnClickListener(this);
        tvbtnClose.setOnClickListener(this);
    }


    private void setUpWithViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ViewAllowanceFragment(), getString(R.string.allowance_report));
        adapter.addFragment(new ViewFragment(), getString(R.string.view_expenses));
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_manu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) item.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);


        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {

                    ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                }
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                expensesListFilter = new ArrayList<>();
                if (searchView != null) {
                    for (ModelExpenses expenses : ViewFragment.addExpensesList) {
                        if (expenses.getTitle().toLowerCase().contains(newText)) {
                            expensesListFilter.add(expenses);
                        }
                        ViewFragment.adapter.filterResulrt(expensesListFilter);
                    }
                }
                if (searchView != null) {
                    allowanceListFilter = new ArrayList<>();
                    for (ModelAllowance addConvince : ViewAllowanceFragment.modelAllowanceList) {
                        if (addConvince.getAmount().contains(newText)) {
                            allowanceListFilter.add(addConvince);
                        }
                        ViewAllowanceFragment.adapter.filterResulrt(allowanceListFilter);
                    }
                } else {

                }


                return true;
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.filter:
                line1.setVisibility(View.GONE);
                builder = new DatePickerBuilder(getActivity(), new OnSelectDateListener() {
                    @Override
                    public void onSelect(List<Calendar> calendarList) {
                        String startDate = null;
                        String endDate = null;

                        try {
                            Calendar calendar = Calendar.getInstance();
                            if (calendarList.size() == 1) {
                                String foriginDate = calendarList.get(0).getTime().toString();
                                SimpleDateFormat formatInF = new SimpleDateFormat("EEE MMM dd hh:mm:ss Z yyyy");
                                SimpleDateFormat formatOutF = new SimpleDateFormat("dd-MM-yyyy");
                                calendar.setTime(formatInF.parse(foriginDate));
                                startDate = formatOutF.format(calendar.getTime());
                                ViewFragment.initDateFilter(startDate, endDate);
                                ViewAllowanceFragment.initDateFilter(startDate, endDate);
                                line1.setVisibility(View.VISIBLE);


                            } else {

                                String foriginDate = calendarList.get(0).getTime().toString();
                                SimpleDateFormat formatInF = new SimpleDateFormat("EEE MMM dd hh:mm:ss Z yyyy");
                                SimpleDateFormat formatOutF = new SimpleDateFormat("dd-MM-yyyy");
                                calendar.setTime(formatInF.parse(foriginDate));
                                startDate = formatOutF.format(calendar.getTime());

                                String soriginDate = calendarList.get(calendarList.size() - 1).getTime().toString();
                                SimpleDateFormat formatInS = new SimpleDateFormat("EEE MMM dd hh:mm:ss Z yyyy");
                                SimpleDateFormat formatOutS = new SimpleDateFormat("dd-MM-yyyy");
                                calendar.setTime(formatInS.parse(soriginDate));
                                endDate = formatOutS.format(calendar.getTime());
                                ViewFragment.initDateFilter(startDate, endDate);
                                ViewAllowanceFragment.initDateFilter(startDate, endDate);
                                line1.setVisibility(View.VISIBLE);

                            }


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                });
                builder.setPickerType(CalendarView.RANGE_PICKER);
                builder.setSelectionColor(R.color.colorPrimary);
                builder.setSelectionLabelColor(R.color.colorBlack);

                datePicker = builder.build();
                datePicker.show();
                return true;
        }
        return super.

                onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v == tvbtnClose) {
            tvStartDate.setText(null);
            tvEndDate.setText(null);
            line1.setVisibility(View.GONE);
            ViewAllowanceFragment.initSetUpView();
            ViewFragment.initSetUpView();
            expensesListFilter.clear();
            allowanceListFilter.clear();
        } else if (v == btnBack) {

            ((HomeActivity) getActivity()).initLoadFragment(new AddConvences());
            HomeActivity.tvExportReport.setVisibility(View.GONE);
            HomeActivity.tvViewReport.setVisibility(View.VISIBLE);


        }
    }
}